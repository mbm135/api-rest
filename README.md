# Backend CRUD API REST

_Ejemplo de WS REST con NodeJS que proporciona un API CRUD para gestionar una DB MongoDB._

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

_Necesitas Chrome o cualquier navegador(preferiblemente que pueda visualizar JSON), Visual Studio Code y Postman._

_Para instalar Chromium(Chrome):_
```
sudo apt update
sudo apt upgrade
sudo apt instal chromium-browser
```
_Para instalar Visual Studio Code(puedes descargarlo desde su página o con los siguientes comandos):_
```
sudo snap install --classic code
```
_Puedes lanzarlo con el siguiente comando o buscar el icono en el explorador:_
```
code .
```
_Para instalar Postman:_
```
sudo snap install postman
```
_Puedes lanzarlo con el siguiente comando o buscar el icono en el explorador:_
```
postman &
```

_Para comenzar con la API vamos a necesitar NodeJS_

_Por lo tanto empezamos con la instalación del gestor de paquetes de Node_

```
sudo apt update
sudo apt install npm
```

_Ahora instalamos una utilidad llamada n para ayudar a la instalación y el mantenimiento de versiones de Node:_

```
sudo npm clean -f
sudo npm i -g n
```

_A continuación instalamos la última versión estable de Node haciendo uso de n:_

```
sudo n stable
```

_Probamos node:_

```
node
>3-2
1
>console.log("¡Hola!");
¡Hola!
>.exit[o Control+c para salir]
```

Seguimo con la instalación de la biblioteca express:

```
npm -i -S express
```

Y desde la terminal con el siguiente comando:

```
node index.js
```

Pondremos en funcionamiento la aplicación.

Si queremos facilitar el uso de la aplicación sin necesidad de tener que reiniciarla constantemente instalaremos nodemon:

```
npm -i -D nodemon
```

Y apartir de ahora cada vez que queramos ejecutar la aplicación usaremos este comando:

```
npm start
```

Para tener un fácil seguimiento de todo lo que hacemos utilizaremos Morgan que es un motor de registros enfocado a Express:

```
npm -i -S morgan
```

Para finalizar introducimos MongoDB:

```
sudo apt update
sudo apt install -y mongodb
```

La gestión del servicio lo realizaremos mediante systemctl:

```
sudo systemctl start mongodb 
```

Abriremos el gestor de base de datos:


```
mongo --host 127.0.0.1:27017
show dbs(para ver las bases de datos)
```

Si queremos trabajar facilmente con mongo en conjunto con node añadiremos dos bibliotecas mongodb(que nos facilita el manejo de la base de datos) y mongojs(que nos ayuda a acceder a mongo a través de node):

```
cd node/nuestra-api
npm i -S mongodb
npm i -S mongojs
```

### Instalación 🔧

Clonamos el proyecto(contraseña para entrar: wrqsCnQz8RKRCmLDUWha):

```
git clone https://mbm135@bitbucket.org/mbm135/api-rest.git
```

## Ejecutando las pruebas ⚙️

_Para poder ejecutar las pruebas simuladas sin MongoDB vamos a Postman e importamos el archivo coleccion.json luego iniciamos en la terminal la aplicación con npm start y vamos enviando una a una las peticiones. Si queremos hacer las pruebas con MongoDb utilizaremos el archivo coleccion-mongo.json_

### Analice las pruebas end-to-end 🔩

Pruebas simuladas para antes de introducir MongoDB:

```
GET http://localhost:3000/api/products
```

Prueba de devolución de todos los productos.
De esta forma podemos verificar si las peticiones para conseguir todos los productos son correctas.

```
GET http://localhost:3000/api/products/258
```

Prueba de devolución de un producto.
De esta forma podemos verificar si las peticiones para conseguir un producto son correctas.

```
POST http://localhost:3000/api/products
```

Prueba de creación de un producto.
De esta forma podemos verificar si las peticiones para crear un producto son correctas.

```
PUT http://localhost:3000/api/products/4298347326
```

Prueba de modificación de un producto.
De esta forma podemos verificar si las peticiones para modificar un producto son correctas.

```
DELETE http://localhost:3000/api/products/38478932
```

Prueba de borrado de un producto.
De esta forma podemos verificar si las peticiones para borrar un producto son correctas.

----

Pruebas después de introducir MongoDB:

```
GET http://localhost:3000/api
```

Prueba de devolución de todos las colecciones.
De esta forma podemos verificar todas las colecciones existenentes en la base de datos.

```
GET http://localhost:3000/api/videosdegatitos
```

Prueba de devolución de los resultados de la colección videosdegatitos.
De esta forma podemos verificar si las peticiones para conseguir todos los resultados de una colección son correctas.

```
POST http://localhost:3000/api/videosdegatitos
```

Prueba de creación de un elemento en la base de datos SD en la colección videosdegatitos.
De esta forma podemos verificar si las peticiones para crear un producto son correctas.

```
PUT http://localhost:3000/api/videosdegatitos/621d0ac4e23b7e1309941927
```

Prueba de modificación de un elemento de la colección videosdegatitos.
De esta forma podemos verificar si las peticiones para modificar un elemento de una colección son correctas.

```
DELETE http://localhost:3000/api/videosdegatitos/621d0ac4e23b7e1309941927
```

Prueba de borrado de un elemento de la colección videosdegatitos.
De esta forma podemos verificar si las peticiones para borrar un elemento de una colección son correctas.

Se recomienda hacer la prueba GET /api/videosdegatitos después de hacer un POST, PUT o DELETE para comprobar los cambios.

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [NodeJS](https://nodejs.org/es/docs/) - El framework web usado
* [Nodemon](https://www.npmjs.com/package/nodemon) - Herramienta
* [Mongodb](https://docs.mongodb.com/) - Base de datos
* [Express](https://expressjs.com/es/) - Infraestructura
* [NPM](https://docs.npmjs.com/) - Gestor de paquetes de Node
* [Visual Studio Code](https://code.visualstudio.com/docs) - IDE
* [Postman](https://www.postman.com/) - API para desarrollar APIs
* [Morgan](https://www.npmjs.com/package/morgan) - Middleware


## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gist.github.com/tu/tuProyecto) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Versionado 📌

Usamos [BitBucket](http://bitbucket.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://bitbucket.org/mbm135/api-rest/commits/).

## Autores ✒️

* **Miguel Bernabéu Morán**  [mbm135](https://bitbucket.org/mbm135/)