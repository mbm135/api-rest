'use strict'

const port = process.env.PORT || 3000

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const cors = require('cors');
const app = express();

const https=require('https');
const fs=require('fs');

const TokenService=require('./services/token.service');

const OPTIONS_HTTPS={
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};


var db = mongojs("SD");
var id = mongojs.ObjectID;

var allowCrossTokenHeader = (req, res, next) => {
    res.header("Access-Control-Allow-Headers", "*");
    return next();
};

var allowCrossTokenOrigin = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    return next();
};

var auth = (req, res, next) => {
    return next();
    if(req.headers.authorization){
        var token= req.headers.authorization.split(' ')[1];
        TokenService.decodificatoken(token)
        .then(user_id => {
            req.user={
                id: user_id,
                token: token
            };
            return next();
        }).catch(err=>{
            res.status(500).json({error_message: err.message});
        })
    }else{
        res.status(400).json({error_message: 'Token vacío, por favor, inserta uno.'});
    }
};

app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(cors());
app.use(allowCrossTokenHeader);
app.use(allowCrossTokenOrigin); 


app.param("coleccion", (req, res, next, coleccion) => {
    console.log('param /api/:coleccion');
    console.log('colección: ', coleccion);

    req.collection = db.collection(coleccion);
    return next();
});

app.get('/api',auth, (req, res, next) => {

    db.getCollectionNames((err, colecciones) => {
            if (err) return next(err);
            res.json(colecciones);
        });
});

app.get('/api/:coleccion',auth, (req, res, next) => {
    req.collection.find((err, coleccion) => {
        if (err) return next(err);
        res.json(coleccion);
    });
});

app.get('/api/:coleccion/:id',auth, (req, res, next) => {
    req.collection.findOne({_id: id(req.params.id)}, (err, elemento) => {
        if (err) return next(err);
        res.json(elemento);
    });
});

app.post('/api/:coleccion', auth, (req, res, next) => {
    const elemento = req.body;
    console.log(elemento);
        req.collection.save(elemento, (err, coleccionGuardada) => {
            if(err) return next(err);
            res.json(coleccionGuardada);
        });
    
});

app.put('/api/:coleccion/:id',auth, (req, res, next) => {
    let elementoId = req.params.id;
    let elementoNuevo = req.body;
    req.collection.update({_id: id(elementoId)},
    {$set: elementoNuevo}, {safe: true, multi: false}, (err, elementoModif) => {
        if (err) return next(err);
        res.json(elementoModif);
    });
});
    
app.delete('/api/:coleccion/:id',auth, (req, res, next) => {
    let elementoId = req.params.id;

    req.collection.remove({_id: id(elementoId)}, (err, resultado) => {
        if (err) return next(err);
        res.json(resultado);
    });
});

https.createServer(OPTIONS_HTTPS,app).listen(port, () => {
    console.log(`SEC WS API REST ejecutándose en http://localhost:${port}/api/:coleccion/:id`);
});