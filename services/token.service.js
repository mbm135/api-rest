'use strict'

const jwt = require('jwt-simple');
const moment=require('moment');

const SECRET=require('../config').secret;
const EXP_TIME=require('../config').tokenExpTmp;

function creatoken(user){
    const payload={
        uid: user._id,
        email:user.email,
        name:user.name,
        pass:user.pass,
        signupdate: user.signupdate,
        lastlogin: user.lastlogin,
        exp: moment().add(EXP_TIME,'minutes').unix()
    };
    return jwt.encode(payload,SECRET);
}

function decodificatoken(token){
    return new Promise((resolve,reject)=>{
        try{
            const payload=jwt.decode(token,SECRET,true);
            if(payload.exp<=moment().unix()){
                reject({
                    status:401,
                    message:'El token ha caducado'
                });
            }
            resolve(payload.uid);
        }catch{
            reject({
                status:500,
                message:'El token no es válido'
            });
        }
    });
}

module.exports={
    creatoken,
    decodificatoken
};